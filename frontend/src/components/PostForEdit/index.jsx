import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Card, Image, Form, Button, Icon } from 'semantic-ui-react';
import moment from 'moment';
import TextareaAutosize from 'react-textarea-autosize';
import * as imageService from 'src/services/imageService';
import { editPost } from 'src/containers/Thread/actions';
import styles from './styles.module.scss';

const PostForEdit = ({ post, editPost: changePost }) => {
  const {
    id,
    image,
    body,
    // user,
    createdAt,
    updatedAt
  } = post;

  const [isUploading, setIsUploading] = useState(false);
  const [currentImage, setImage] = useState(image);
  const [newBody, setBody] = useState(body);
  const uploadImage = file => imageService.uploadImage(file);

  const handleUploadFile = async ({ target }) => {
    setIsUploading(true);
    try {
      const { id: imageId, link: imageLink } = await uploadImage(target.files[0]);
      setImage({ id: imageId, link: imageLink });
    } finally {
      setIsUploading(false);
    }
  };

  const handleDeleteImage = async () => {
    try {
      setImage(undefined);
    } finally {
      setIsUploading(false);
    }
  };

  const handleEditPost = async () => {
    if (newBody === body && currentImage === image) {
      return;
    }
    document.body.click();
    await changePost({ imageId: currentImage?.id, body: newBody, id });
    setBody('');
    setImage(undefined);
  };

  const EditableTextArea = text => (
    <TextareaAutosize
      style={{ wordWrap: 'break-word' }}
      autoFocus
      defaultValue={text}
      onChange={e => setBody(e.currentTarget.value)}
    />
  );

  const date = moment(createdAt).fromNow();
  const updDate = moment(updatedAt).fromNow();

  const delButton = isImage => (
    isImage ? (
      <Button color="pink" icon labelPosition="left" compact as="label" onClick={handleDeleteImage}>
        <Icon name="trash alternate outline" />
        Delete image
      </Button>
    ) : '');

  return (
    <Card style={{ width: '100%' }}>
      {currentImage?.link && (
        <div className={styles.imageWrapper}>
          <Image className={styles.image} src={currentImage?.link} alt="post" />
        </div>
      )}
      <div>
        <Button color="teal" icon labelPosition="left" as="label" compact loading={isUploading} disabled={isUploading}>
          <Icon name="image" />
          Add new image
          <input name="image" type="file" onChange={handleUploadFile} hidden />
        </Button>
        {delButton(currentImage)}
      </div>
      <Card.Content>
        <Card.Meta>
          <span className="date">
            {date === updDate
              ? `posted - ${date}`
              : `updated - ${updDate}`}
          </span>
        </Card.Meta>
        <Card.Description>
          <Form>
            {EditableTextArea(newBody)}
          </Form>
        </Card.Description>
        <Button floated="right" color="blue" onClick={handleEditPost}>Save Changes</Button>
      </Card.Content>
    </Card>
  );
};

PostForEdit.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  editPost: PropTypes.func.isRequired
};

const actions = { editPost };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  null,
  mapDispatchToProps
)(PostForEdit);
