import { Popup, Image, Segment } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import React from 'react';

const ReactionPopup = ({ reactions, type, element }) => {
  const specReactions = type === 'like' ? reactions.filter(r => r.isLike) : reactions.filter(r => r.isDislike);
  if (typeof specReactions !== 'undefined' && specReactions.length > 0) {
    const UserIcon = reaction => (reaction?.userAvatarLink
      ? reaction?.userAvatarLink : 'https://forwardsummit.ca/wp-content/uploads/2019/01/avatar-default.png');

    const userIconPopUp = reaction => (
      <Popup key={`${type}${reaction.userId}`} basic trigger={<Image src={UserIcon(reaction)} avatar size="mini" />}>
        {reaction.userName}
      </Popup>
    );

    const diff = specReactions.length > 5 ? specReactions.length - 5 : 0;
    specReactions.length -= diff;

    return (
      <Popup
        inverted
        hideOnScroll
        basic
        offset="200"
        mouseLeaveDelay={300}
        mouseEnterDelay={600}
        on="hover"
        hoverable
        wide
        trigger={element}
      >
        <Segment textAlign="center">{specReactions.map(reaction => userIconPopUp(reaction))}</Segment>
        <div>{diff > 0 ? `and ${diff} more ${type} it` : `${type} it`}</div>
      </Popup>
    );
  }
  return element;
};

ReactionPopup.propTypes = {
  element: PropTypes.element.isRequired,
  type: PropTypes.string.isRequired,
  reactions: PropTypes.arrayOf(PropTypes.object)
};

ReactionPopup.defaultProps = {
  reactions: []
};

export default ReactionPopup;
