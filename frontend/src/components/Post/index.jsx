import React from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon } from 'semantic-ui-react';
import moment from 'moment';
import ReactionPopup from 'src/components/ReactionPopup';
import styles from './styles.module.scss';

const Post = ({ post, likePost, dislikePost, toggleExpandedPost,
  toggleEditablePost, togglePostDelete, sharePost, currentUserId }) => {
  const {
    reactions = [],
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt
  } = post;

  const date = moment(createdAt).fromNow();
  return (
    <Card style={{ width: '100%' }} id={id}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
        </Card.Meta>
        <Card.Description style={{ wordWrap: 'break-word' }}>
          {body}
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <ReactionPopup
          reactions={reactions}
          type="like"
          element={
            (
              <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likePost(id)}>
                <Icon name="thumbs up" />
                {likeCount === 0 ? '\u2007' : likeCount}
              </Label>
            )
          }
        />
        <ReactionPopup
          reactions={reactions}
          type="dislike"
          element={
            (
              <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => dislikePost(id)}>
                <Icon name="thumbs down" />
                {dislikeCount === 0 ? '\u2007' : dislikeCount}
              </Label>
            )
          }
        />
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
          <Icon name="comment" />
          {commentCount === 0 ? '\u2007' : commentCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
          <Icon name="share alternate" />
        </Label>
        {post.user.id === currentUserId
          ? (
            <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleEditablePost(id)}>
              <Icon name="edit" />
            </Label>
          ) : ''}
        {post.user.id === currentUserId
          ? (
            <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => togglePostDelete(id)}>
              <Icon name="delete" />
            </Label>
          ) : ''}
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  toggleEditablePost: PropTypes.func,
  togglePostDelete: PropTypes.func,
  sharePost: PropTypes.func.isRequired,
  currentUserId: PropTypes.string
};

Post.defaultProps = {
  currentUserId: undefined,
  toggleEditablePost: undefined,
  togglePostDelete: undefined
};

export default Post;
