import { Dropdown, Button, Icon } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import React, { useState, createRef } from 'react';
import * as emailService from 'src/services/emailService';
import * as userService from 'src/services/authService';

const SharePostDrop = ({ link }) => {
  const selectedRef = createRef();
  const [isShareble, setIsShareble] = useState(false);
  const [users, setUsers] = useState([]);
  const handleSelect = () => {
    userService.getUserNames().then(
      result => {
        setUsers(result
          .map(el => {
            const elem = { key: el.id, text: el.username, value: el.username };
            return elem;
          }));
      }
    );
    setIsShareble(!isShareble);
  };

  const handleShare = async () => {
    const selectedIdToEmail = users
      .filter(entry => selectedRef.current.state.value.includes(entry.value)).map(entry => entry.key);
    setIsShareble(!isShareble);
    await emailService
      .sharePostByEmail({ postLink: link, receiversId: selectedIdToEmail });
  };

  return (
    <div>
      {isShareble ? '' : (
        <Button icon onClick={handleSelect} color="linkedin">
          <Icon name="mail" />
          Share with email
        </Button>
      )}
      {!isShareble ? '' : (
        <Dropdown
          ref={selectedRef}
          multiple
          open
          placeholder="Choose e-mail"
          options={users}
        />
      )}
      {!isShareble ? '' : <Button onClick={handleShare} color="linkedin">Send e-mail</Button>}
    </div>
  );
};

SharePostDrop.propTypes = {
  link: PropTypes.string.isRequired
};

export default SharePostDrop;
