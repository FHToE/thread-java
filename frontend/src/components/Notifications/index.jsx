import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Stomp } from '@stomp/stompjs';
import SockJS from 'sockjs-client';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import 'react-notifications/lib/notifications.css';

const Notifications = ({ user, applyPost }) => {
  const [stompClient] = useState(Stomp.over(new SockJS('/ws')));

  useEffect(() => {
    if (!user) {
      return undefined;
    }

    stompClient.debug = () => { };
    stompClient.connect({}, () => {
      console.log('connected');

      const { id } = user;

      stompClient.subscribe('/topic/like', message => {
        const reaction = JSON.parse(message.body);
        if (reaction.postUserId === id) {
          NotificationManager.info('Your post was liked!');
        }
      });

      stompClient.subscribe('/topic/dislike', message => {
        const reaction = JSON.parse(message.body);
        if (reaction.postUserId === id) {
          NotificationManager.info('Your post was disliked!');
        }
      });

      stompClient.subscribe('/topic/edit_post', message => {
        if (JSON.parse(message.body) === id) {
          NotificationManager.info('Your post has been updated!');
        }
      });

      stompClient.subscribe('/topic/delete_post', message => {
        if (JSON.parse(message.body) === id) {
          NotificationManager.info('Your post has been deleted!');
        }
      });

      stompClient.subscribe('/topic/edit_comment', message => {
        if (JSON.parse(message.body) === id) {
          NotificationManager.info('Your comment has been updated!');
        }
      });

      stompClient.subscribe('/topic/delete_comment', message => {
        if (JSON.parse(message.body) === id) {
          NotificationManager.info('Your comment has been deleted!');
        }
      });

      stompClient.subscribe('/topic/add_comment', message => {
        if (JSON.parse(message.body) === id) {
          NotificationManager.info('Someone commmented your post! Check it');
        }
      });

      stompClient.subscribe('/topic/commentlike', message => {
        if (JSON.parse(message.body) === id) {
          NotificationManager.info('Your comment was liked');
        }
      });

      stompClient.subscribe('/topic/commentdislike', message => {
        if (JSON.parse(message.body) === id) {
          NotificationManager.info('Your comment was disliked');
        }
      });

      stompClient.subscribe('/topic/new_post', message => {
        const post = JSON.parse(message.body);
        if (post.userId !== id) {
          applyPost(post.id);
        }
      });
    });

    return () => {
      stompClient.disconnect();
    };
  });

  return <NotificationContainer />;
};

Notifications.defaultProps = {
  user: undefined
};

Notifications.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  applyPost: PropTypes.func.isRequired
};

export default Notifications;
