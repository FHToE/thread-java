import React from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Label, Icon, Popup } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';
import ReactionPopup from 'src/components/ReactionPopup';

import styles from './styles.module.scss';

const Comment = ({
  id,
  toggleEditableComment,
  toggleCommentDelete,
  currentUserId,
  comment,
  likeComment,
  dislikeComment
}) => {
  const { body, createdAt, user, likeCount, dislikeCount, reactions = [] } = comment;
  return (
    <CommentUI id={id} className={styles.comment}>
      <CommentUI.Avatar src={getUserImgLink(user.avatar)} />
      <CommentUI.Content>
        <CommentUI.Author as="a">
          {user.username}
        </CommentUI.Author>
        <CommentUI.Metadata>
          {moment(createdAt).fromNow()}
        </CommentUI.Metadata>
        <br />
        <CommentUI.Metadata>
          {user.status ? `${user.status.substring(0, 100)}` : ''}
          {user.status?.length > 100 ? '...' : '' }
        </CommentUI.Metadata>
        <CommentUI.Text>
          {body}
        </CommentUI.Text>
        <CommentUI.Actions>
          <ReactionPopup
            reactions={reactions}
            type="like"
            element={
              (
                <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likeComment(id)}>
                  <Icon name="thumbs up" />
                  {likeCount === 0 ? '\u2007' : likeCount}
                </Label>
              )
            }
          />
          <ReactionPopup
            reactions={reactions}
            type="dislike"
            element={
              (
                <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => dislikeComment(id)}>
                  <Icon name="thumbs down" />
                  {dislikeCount === 0 ? '\u2007' : dislikeCount}
                </Label>
              )
            }
          />
        </CommentUI.Actions>
        {user.id === currentUserId
          ? (
            <div>
              <Popup wide trigger={<Icon name="ellipsis horizontal" />} on="click">
                <Popup
                  trigger={
                    (
                      <Label
                        basic
                        size="small"
                        as="a"
                        className={styles.OwnerBtn}
                        onClick={() => toggleEditableComment(id)}
                      >
                        <Icon name="pencil alternate" />
                      </Label>
                    )
                  }
                  content="Edit my comment"
                  position="top center"
                  size="tiny"
                  inverted
                />
                <Popup
                  trigger={
                    (
                      <Label
                        basic
                        size="small"
                        as="a"
                        className={styles.OwnerBtn}
                        onClick={() => toggleCommentDelete(id)}
                      >
                        <Icon name="trash alternate outline" />
                      </Label>
                    )
                  }
                  content="Delete my comment"
                  position="top center"
                  size="tiny"
                  inverted
                />
              </Popup>
            </div>
          ) : ''}
      </CommentUI.Content>
    </CommentUI>
  );
};

Comment.propTypes = {
  toggleCommentDelete: PropTypes.func,
  toggleEditableComment: PropTypes.func,
  id: PropTypes.string.isRequired,
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  currentUserId: PropTypes.string.isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired
};

Comment.defaultProps = {
  toggleEditableComment: undefined,
  toggleCommentDelete: undefined
};

export default Comment;
