import React, { useState } from 'react';
import { Form, Button, Segment, Label } from 'semantic-ui-react';
import * as userService from 'src/services/authService';

const ResetPasswordForm = () => {
  const [email, setEmail] = useState(undefined);
  const [isValidEmail, setisValidEmail] = useState(true);
  const [result, setResult] = useState(undefined);
  const [isUploading, setIsUploading] = useState(false);

  function validateEmail(e) {
    const pattern = /^([A-Za-z0-9_\-.])+@([A-Za-z0-9_\-.])+\.([A-Za-z]{2,4})$/;
    setEmail(e.target.value);
    setisValidEmail(pattern.test(e.target.value));
  }

  const handleLoginClick = async () => {
    setIsUploading(true);
    setResult(undefined);
    const res = await userService.emailResetPassword({ email });
    if (res) {
      setResult('You`ve got reset link on your e-mail. Follow it.');
    } else {
      setResult('This e-mail doesn`t exist.');
    }
    setIsUploading(false);
  };

  return (
    <Form size="large" onSubmit={handleLoginClick}>
      <Segment>
        {isValidEmail ? <br /> : <Label color="pink" size="mini">Not valid email</Label>}
        <Form.Input
          fluid
          icon="at"
          iconPosition="left"
          placeholder="Email"
          type="email"
          onChange={ev => validateEmail(ev)}
        />
        {(result && result === 'You`ve got reset link on your e-mail. Follow it.') ? '' : (
          <Button type="submit" disabled={!isValidEmail} loading={isUploading} color="teal" fluid size="large" primary>
            Send reset link on e-mail
          </Button>
        )}
        {result ? <Label color="violet" size="small">{result}</Label> : ''}
      </Segment>
    </Form>
  );
};

export default ResetPasswordForm;
