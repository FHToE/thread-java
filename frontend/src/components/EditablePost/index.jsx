import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal } from 'semantic-ui-react';
import { toggleEditablePost } from 'src/containers/Thread/actions';
import PostForEdit from 'src/components/PostForEdit';
import Spinner from 'src/components/Spinner';

const EditablePost = ({
  post,
  toggleEditablePost: toggleEdit
}) => (
  <Modal dimmer="blurring" centered={false} closeOnDocumentClick open onClose={() => toggleEdit()} closeIcon>
    <Modal.Header>Here you can modify your post</Modal.Header>
    {post
      ? (
        <Modal.Content>
          <PostForEdit
            post={post}
          />
        </Modal.Content>
      )
      : <Spinner />}
  </Modal>
);

EditablePost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleEditablePost: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  post: rootState.posts.editablePost
});

const actions = { toggleEditablePost };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditablePost);
