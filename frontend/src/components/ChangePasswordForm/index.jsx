import React, { useState } from 'react';
import { Form, Button, Segment, Label } from 'semantic-ui-react';
import * as userService from 'src/services/authService';
import styles from './styles.module.scss';

const ChangePasswordForm = link => {
  const [password, setPassword] = useState('');
  const [isPasswordValid, setIsPasswordValid] = useState(true);
  const [isValidLink, setIsValidLink] = useState(undefined);
  const [result, setResult] = useState(undefined);
  const [resultLabel, setResultLabel] = useState(undefined);
  const [isUploading, setIsUploading] = useState(false);

  const passwordChanged = value => {
    setPassword(value);
    setIsPasswordValid(true);
  };

  const handleChange = async () => {
    setIsUploading(true);
    const res = await userService.saveNewPassword({ password, resetId: link.id });
    setResult(res);
    if (res) {
      setResultLabel(<Label color="green" size="small">Saved! Go to start page and login...</Label>);
    } else {
      setResultLabel(
        <Label
          color="pink"
          size="small"
        >
          Oops. Something went wrong. Probably, timelife of your link have been expired.
        </Label>
      );
    }
    setIsUploading(false);
  };

  const checkLink = async () => {
    const res = await userService.isResetLinkValid(link.id);
    setIsValidLink(res);
  };

  const inputElement = isValidLink ? (
    <div>
      <Form.Input
        fluid
        icon="lock"
        iconPosition="left"
        placeholder="Password"
        type="password"
        onChange={ev => passwordChanged(ev.target.value)}
        error={!isPasswordValid}
        onBlur={() => setIsPasswordValid(Boolean(password))}
      />
      <Button
        onClick={handleChange}
        loading={isUploading}
        disabled={!isPasswordValid}
        color="teal"
        fluid
        size="large"
        primary
      >
        Save password
      </Button>
    </div>
  ) : (
    <Label color="pink" size="large">
      Current link isnt valid to change Password
    </Label>
  );

  return (
    <Form size="large">
      <Segment>
        {isValidLink !== undefined ? '' : (
          <Label
            content="Set new password..."
            onClick={checkLink}
            basic
            size="large"
            as="a"
            className={styles.toolbarBtn}
          />
        )}
        {(result === undefined && isValidLink !== undefined) ? inputElement : '' }
        {result !== undefined ? resultLabel : ''}
      </Segment>
    </Form>
  );
};

export default ChangePasswordForm;
