import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Button } from 'semantic-ui-react';
import { deleteComment, toggleCommentDelete } from 'src/containers/Thread/actions';
import Spinner from 'src/components/Spinner';

const ModalDeleteComment = ({
  comment,
  toggleCommentDelete: toggleDel,
  deleteComment: delComment
}) => {
  const {
    body
  } = comment;

  const handleDeleteComment = async () => {
    await delComment(comment);
    document.body.click();
  };

  return (
    <Modal dimmer="blurring" size="mini" open closeOnDocumentClick closeIcon onClose={() => toggleDel()}>
      <Modal.Header>Are you sure you want to delete your Comment</Modal.Header>
      <Modal.Content>
        <Modal.Description>
          {body}
        </Modal.Description>
      </Modal.Content>
      {comment
        ? (
          // <Modal.Content />
          // </Modal.Content>
          <Modal.Actions>
            <Button
              onClick={handleDeleteComment}
              negative
              labelPosition="right"
              icon="checkmark"
              content="Yes"
            />
          </Modal.Actions>
        )
        : <Spinner />}
    </Modal>
  );
};

ModalDeleteComment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleCommentDelete: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  comment: rootState.posts.deleteCommentModal
});

const actions = { deleteComment, toggleCommentDelete };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ModalDeleteComment);
