import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Button } from 'semantic-ui-react';
import { deletePost, togglePostDelete } from 'src/containers/Thread/actions';
import Spinner from 'src/components/Spinner';

const ModalDeletePost = ({
  post,
  togglePostDelete: toggleDel,
  deletePost: delPost
}) => {
  const handleDeletePost = async () => {
    await delPost(post.id);
    document.body.click();
  };
  return (
    <Modal dimmer="blurring" open size="small" closeIcon closeOnDocumentClick onClose={() => toggleDel()}>
      <Modal.Header>Are you sure you want to delete your Post? </Modal.Header>
      <Modal.Content>
        <Modal.Description>
          {post.body}
        </Modal.Description>
      </Modal.Content>
      {post
        ? (
          // <Modal.Content />
          // </Modal.Content>
          <Modal.Actions>
            <Button
              onClick={handleDeletePost}
              negative
              labelPosition="right"
              icon="checkmark"
              content="Yes"
            />
          </Modal.Actions>
        )
        : <Spinner />}
    </Modal>
  );
};

ModalDeletePost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  togglePostDelete: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  post: rootState.posts.deletePostModal
});

const actions = { deletePost, togglePostDelete };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ModalDeletePost);
