import React, { useState } from 'react';
import { Label, Icon, Button } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import TextareaAutosize from 'react-textarea-autosize';
import { updateUserStatus } from 'src/containers/Profile/actions';
import styles from './styles.module.scss';

const StatusInputField = ({ user, updateUserStatus: update }) => {
  const { status } = user;
  const [currentStatus, setCurrent] = useState(status);
  const [newStatus, setStatus] = useState(currentStatus);
  const [isEditableStatus, setIsEditableStatus] = useState(false);

  const handleUpdate = async () => {
    await update({
      status: newStatus,
      id: user.id
    }).then(() => {
      setCurrent(newStatus);
      setIsEditableStatus(false);
    });
  };

  return (
    <div>
      {isEditableStatus ? (
        <div>
          <Label
            basic
            size="tiny"
            as="a"
            className={styles.toolbarBtn}
            onClick={() => {
              setStatus(currentStatus);
              setIsEditableStatus(!isEditableStatus);
            }}
          >
            <Icon color="pink" name="undo">
              <div>cancel</div>
            </Icon>
          </Label>
          <Button
            compact
            color="teal"
            size="small"
            disabled={newStatus === currentStatus}
            onClick={handleUpdate}
          >
            Save
          </Button>
        </div>
      )
        : (
          <div>
            <Label
              basic
              size="tiny"
              as="a"
              className={styles.toolbarBtn}
              onClick={() => setIsEditableStatus(!isEditableStatus)}
            >
              <Icon color="black" name="pencil alternate">
                set new
              </Icon>
            </Label>
          </div>
        ) }
      {isEditableStatus ? (
        <TextareaAutosize
          rows="4"
          style={{ wordWrap: 'break-word' }}
          autoFocus
          defaultValue={user.status}
          onChange={e => setStatus(e.currentTarget.value)}
        />
      )
        : (
          <Label
            content={user.status}
            onClick={() => setIsEditableStatus(!isEditableStatus)}
            basic
            size="large"
            as="a"
            className={styles.toolbarBtn}
          />
        )}
    </div>
  );

  /* return (
    <div>
      My status:
      <br />
      <Input
        icon="hand pointer"
        iconPosition="left"
        placeholder="Your status"
        type="text"
        disabled={!isEditableStatus}
        value={newStatus}
        onChange={e => setStatus(e.currentTarget.value)}
      />
      {!isEditableStatus ? (
        <Label
          basic
          size="tiny"
          as="a"
          className={styles.toolbarBtn}
          onClick={() => setIsEditableStatus(!isEditableStatus)}
        >
          <Icon color="black" name="pencil alternate">
            <div>set new</div>
          </Icon>
        </Label>
      )
        : (
          <Label
            basic
            size="tiny"
            as="a"
            className={styles.toolbarBtn}
            onClick={() => {
              setStatus(currentStatus);
              setIsEditableStatus(!isEditableStatus);
            }}
          >
            <Icon color="pink" name="undo">
              <div>cancel</div>
            </Icon>
          </Label>
        )}
      {newStatus === currentStatus ? ''
        : <Button compact color="teal" size="small" onClick={handleUpdate}>Save</Button>}
    </div>
  );*/
};

StatusInputField.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  updateUserStatus: PropTypes.func.isRequired
};

StatusInputField.defaultProps = {
  user: {}
};

const actions = { updateUserStatus };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

const mapStateToProps = rootState => ({
  user: rootState.profile.user
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StatusInputField);
