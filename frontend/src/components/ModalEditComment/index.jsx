import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Modal, Button, Card, Form } from 'semantic-ui-react';
import TextareaAutosize from 'react-textarea-autosize';
import Spinner from 'src/components/Spinner';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { toggleEditableComment, editComment } from 'src/containers/Thread/actions';

const ModalEditComment = ({
  postId,
  comment,
  toggleEditableComment: toggle,
  editComment: changeComment
}) => {
  const {
    id,
    body
  } = comment;
  const [newBody, setBody] = useState(body);

  const handleEdit = () => {
    document.body.click();
    changeComment({ postId, body: newBody, id });
  };

  const EditableTextArea = text => (
    <TextareaAutosize
      style={{ wordWrap: 'break-word' }}
      autoFocus
      defaultValue={text}
      onChange={e => setBody(e.currentTarget.value)}
    />
  );

  return (
    <Modal
      closeOnDocumentClick
      dimmer="blurring"
      style={{ width: '50%' }}
      closeIcon
      centered
      open
      onClose={() => toggle()}
    >
      <Modal.Header>Here you can modify your comment</Modal.Header>
      {comment
        ? (
          <Modal.Content>
            <Card style={{ width: '100%' }}>
              <Card.Content>
                <Card.Description>
                  <Form>
                    {EditableTextArea(newBody)}
                  </Form>
                </Card.Description>
                <Button floated="right" color="blue" compact onClick={handleEdit}>Save Changes</Button>
              </Card.Content>
            </Card>
          </Modal.Content>
        )
        : <Spinner />}
    </Modal>
  );
};

ModalEditComment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleEditableComment: PropTypes.func.isRequired,
  editComment: PropTypes.func.isRequired,
  postId: PropTypes.string.isRequired
};

const mapStateToProps = rootState => ({
  comment: rootState.posts.editableComment
});

const actions = { toggleEditableComment, editComment };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ModalEditComment);
