import callWebApi from 'src/helpers/webApiHelper';

export const likePostEmail = async request => {
  await callWebApi({
    endpoint: `/api/email/${request.postId}`,
    type: 'POST',
    request
  });
};

export const sharePostByEmail = async request => {
  await callWebApi({
    endpoint: '/api/email/sharepost',
    type: 'POST',
    request
  });
};
