import callWebApi from 'src/helpers/webApiHelper';

export const addComment = async request => {
  const response = await callWebApi({
    endpoint: '/api/comments',
    type: 'POST',
    request
  });
  return response.json();
};

export const editComment = async request => {
  const response = await callWebApi({
    endpoint: `/api/comments/${request.id}`,
    type: 'POST',
    request
  });
  return response.json();
};

export const deleteComment = async id => callWebApi({ endpoint: `/api/comments/delete/${id}`, type: 'POST' });

export const getComment = async id => {
  const response = await callWebApi({
    endpoint: `/api/comments/${id}`,
    type: 'GET'
  });
  return response.json();
};

export const likeComment = async commentId => {
  const response = await callWebApi({
    endpoint: '/api/commentreaction',
    type: 'PUT',
    request: {
      commentId,
      isLike: true,
      isDislike: false
    }
  });
  return response.json();
};

export const getReactions = async commentId => {
  const response = await callWebApi({
    endpoint: `/api/comments/reactions/${commentId}`,
    type: 'GET'
  });
  return response.json();
};

export const dislikeComment = async commentId => {
  const response = await callWebApi({
    endpoint: '/api/commentreaction',
    type: 'PUT',
    request: {
      commentId,
      isLike: false,
      isDislike: true
    }
  });
  return response.json();
};
