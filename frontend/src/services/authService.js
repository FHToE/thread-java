import callWebApi from 'src/helpers/webApiHelper';

export const login = async request => {
  const response = await callWebApi({
    endpoint: '/api/auth/login',
    type: 'POST',
    request
  });
  return response.json();
};

export const registration = async request => {
  const response = await callWebApi({
    endpoint: '/api/auth/register',
    type: 'POST',
    request
  });
  return response.json();
};

export const getCurrentUser = async () => {
  try {
    const response = await callWebApi({
      endpoint: '/api/user',
      type: 'GET'
    });
    return response.json();
  } catch (e) {
    return null;
  }
};

export const getUserNames = async () => {
  try {
    const response = await callWebApi({
      endpoint: '/api/user/getall',
      type: 'GET'
    });
    return response.json();
  } catch (e) {
    return null;
  }
};

export const updateUserStatus = async request => {
  await callWebApi({
    endpoint: '/api/user/status',
    type: 'POST',
    request
  });
};

export const emailResetPassword = async request => {
  const response = await callWebApi({
    endpoint: '/api/user/getreset',
    type: 'POST',
    request
  });
  return response.json();
};

export const saveNewPassword = async request => {
  const response = await callWebApi({
    endpoint: '/api/user/getreset',
    type: 'PUT',
    request
  });
  return response.json();
};

export const isResetLinkValid = async id => {
  const response = await callWebApi({
    endpoint: `/api/user/getreset/${id}`,
    type: 'GET'
  });
  return response.json();
};

export const editUserData = async request => {
  try {
    const response = await callWebApi({
      endpoint: `/api/user/${request.id}`,
      type: 'POST',
      request
    });
    return response.json();
  } catch (e) {
    return null;
  }
};
