import * as postService from 'src/services/postService';
import * as emailService from 'src/services/emailService';
import * as commentService from 'src/services/commentService';
import {
  ADD_POST,
  LOAD_MORE_POSTS,
  SET_ALL_POSTS,
  SET_EXPANDED_POST,
  SET_EDITABLE_POST,
  SET_DELETE_POST,
  SET_EDITABLE_COMMENT,
  SET_DELETE_COMMENT
} from './actionTypes';

const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

const addMorePostsAction = posts => ({
  type: LOAD_MORE_POSTS,
  posts
});

const addPostAction = post => ({
  type: ADD_POST,
  post
});

const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});

const setEditablePostAction = post => ({
  type: SET_EDITABLE_POST,
  post
});

const setEditableCommentAction = comment => ({
  type: SET_EDITABLE_COMMENT,
  comment
});

const setDeleteCommentAction = comment => ({
  type: SET_DELETE_COMMENT,
  comment
});

const setDeletePostAction = post => ({
  type: SET_DELETE_POST,
  post
});

export const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
  const { posts: { posts } } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts
    .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
  dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};

export const deletePost = postId => async (dispatch, getRootState) => {
  const delPost = await postService.getPost(postId);
  await postService.deletePost(postId);
  const { posts: { posts } } = getRootState();
  const updated = posts.filter(post => post.id !== delPost.id);
  dispatch(setPostsAction(updated));
};

export const addPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPostAction(newPost));
};

export const editPost = post => async (dispatch, getRootState) => {
  const { id } = await postService.editPost(post);
  const newPost = await postService.getPost(id);
  const { posts: { posts } } = getRootState();
  const mapEdit = thisPost => ({
    ...thisPost,
    body: newPost.body,
    image: newPost.image
  });
  const updated = posts.map(thisPost => (thisPost.id !== id
    ? thisPost
    : mapEdit(thisPost)));
  dispatch(setPostsAction(updated));
};

export const deleteComment = comment => async (dispatch, getRootState) => {
  await commentService.deleteComment(comment.id);
  const { posts: { posts, expandedPost } } = getRootState();

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) - 1 // diff is taken from the current closure
  });

  const updated = posts.map(post => (post.id !== comment.postId ? post : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
  try {
    document.getElementById(comment.id).remove();
  } catch (err) {
    //
  }
};

export const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPostAction(post));
};

export const togglePostDelete = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setDeletePostAction(post));
};

export const toggleEditablePost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setEditablePostAction(post));
};

export const toggleEditableComment = commentId => async dispatch => {
  const comment = commentId ? await commentService.getComment(commentId) : undefined;
  dispatch(setEditableCommentAction(comment));
};

export const toggleCommentDelete = commentId => async dispatch => {
  const comment = commentId ? await commentService.getComment(commentId) : undefined;
  dispatch(setDeleteCommentAction(comment));
};

export const likePost = postId => async (dispatch, getRootState) => {
  const result = await postService.likePost(postId);
  const diff = result.isLike ? 1 : -1;

  // if response return that post was liked - increment counter, otherwise - decrement
  const reactions = await postService.getReactions(postId);
  const mapLikes = post => ({
    ...post,
    likeCount: Number(post.likeCount) + diff, // diff is taken from the current closure
    reactions
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapLikes(expandedPost)));
  }
  if (result.isLike && result.userId !== result.postUserId) {
    await emailService
      .likePostEmail({ postId: result.postId, postOwnerId: result.postUserId, postLikedById: result.userId });
  }
};

export const dislikePost = postId => async (dispatch, getRootState) => {
  const result = await postService.dislikePost(postId);
  const diff = result.isDislike ? 1 : -1;
  // if response return that post was disliked - increment counter, otherwise - decrement
  const reactions = await postService.getReactions(postId);

  const mapDislikes = post => ({
    ...post,
    dislikeCount: Number(post.dislikeCount) + diff, // diff is taken from the current closure
    reactions
  });

  const { posts: { posts, expandedPost } } = getRootState();

  const updated = posts.map(post => (post.id !== postId ? post : mapDislikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapDislikes(expandedPost)));
  }
};

export const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));
  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const editComment = comment => async (dispatch, getRootState) => {
  const { id } = await commentService.editComment(comment);
  const newComment = await commentService.getComment(id);
  const { posts: { expandedPost } } = getRootState();

  const mapEdit = thisComment => ({
    ...thisComment,
    body: newComment.body
  });

  const updated = expandedPost.comments.map(thisComment => (thisComment.id !== id
    ? thisComment
    : mapEdit(thisComment)));

  const mapComments = post => ({
    ...post,
    comments: updated
  });
  dispatch(setExpandedPostAction(mapComments(expandedPost)));
};

export const likeComment = commentId => async (dispatch, getRootState) => {
  const result = await commentService.likeComment(commentId);
  const diff = result.isLike ? 1 : -1;
  // if response return that comment was liked - increment counter, otherwise - decrement
  const reactions = await commentService.getReactions(commentId);

  const mapLikes = comment => ({
    ...comment,
    likeCount: Number(comment.likeCount) + diff, // diff is taken from the current closure
    reactions
  });

  const { posts: { expandedPost } } = getRootState();
  const updated = expandedPost.comments.map(comment => (comment.id !== commentId ? comment : mapLikes(comment)));

  const mapComments = post => ({
    ...post,
    comments: updated
  });

  dispatch(setExpandedPostAction(mapComments(expandedPost)));
};

export const dislikeComment = commentId => async (dispatch, getRootState) => {
  const result = await commentService.dislikeComment(commentId);
  const diff = result.isDislike ? 1 : -1;
  // if response return that comment was liked - increment counter, otherwise - decrement
  const reactions = await commentService.getReactions(commentId);

  const mapDislikes = comment => ({
    ...comment,
    dislikeCount: Number(comment.dislikeCount) + diff, // diff is taken from the current closure
    reactions
  });

  const { posts: { expandedPost } } = getRootState();

  const updated = expandedPost.comments.map(comment => (comment.id !== commentId ? comment : mapDislikes(comment)));

  const mapComments = post => ({
    ...post,
    comments: updated
  });

  dispatch(setExpandedPostAction(mapComments(expandedPost)));
};
