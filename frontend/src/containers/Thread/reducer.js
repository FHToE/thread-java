import {
  SET_ALL_POSTS,
  LOAD_MORE_POSTS,
  ADD_POST,
  SET_EXPANDED_POST,
  SET_EDITABLE_POST,
  SET_DELETE_POST,
  SET_EDITABLE_COMMENT,
  SET_DELETE_COMMENT
} from './actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case SET_ALL_POSTS:
      return {
        ...state,
        posts: action.posts,
        hasMorePosts: Boolean(action.posts.length)
      };
    case LOAD_MORE_POSTS:
      return {
        ...state,
        posts: [...(state.posts || []), ...action.posts],
        hasMorePosts: Boolean(action.posts.length)
      };
    case ADD_POST:
      return {
        ...state,
        posts: [action.post, ...state.posts]
      };
    case SET_EXPANDED_POST:
      return {
        ...state,
        expandedPost: action.post
      };
    case SET_DELETE_POST:
      return {
        ...state,
        deletePostModal: action.post
      };
    case SET_EDITABLE_POST:
      return {
        ...state,
        editablePost: action.post
      };
    case SET_DELETE_COMMENT:
      return {
        ...state,
        deleteCommentModal: action.comment
      };
    case SET_EDITABLE_COMMENT:
      return {
        ...state,
        editableComment: action.comment
      };
    default:
      return state;
  }
};
