/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as imageService from 'src/services/imageService';
import ExpandedPost from 'src/containers/ExpandedPost';
import EditablePost from 'src/components/EditablePost';
import ModalDeletePost from 'src/components/ModalDeletePost';
import Post from 'src/components/Post';
import AddPost from 'src/components/AddPost';
import SharedPostLink from 'src/components/SharedPostLink';
import { Checkbox, Loader, Grid } from 'semantic-ui-react';
import InfiniteScroll from 'react-infinite-scroller';
import { loadPosts, loadMorePosts, likePost, dislikePost, toggleExpandedPost,
  toggleEditablePost, togglePostDelete, addPost } from './actions';

import styles from './styles.module.scss';

const postsFilter = {
  from: 0,
  count: 10,
  ignoreUser: false,
  ignoreOther: false,
  likedByMe: false
};

export const Thread = ({
  userId,
  loadPosts: load,
  loadMorePosts: loadMore,
  posts = [],
  expandedPost,
  editablePost,
  deletePostModal,
  hasMorePosts,
  addPost: createPost,
  likePost: like,
  dislikePost: dislike,
  toggleExpandedPost: toggle,
  toggleEditablePost: toggleEdit,
  togglePostDelete: toggleDelete
}) => {
  const [sharedPostId, setSharedPostId] = useState(undefined);
  const [showOwnPosts, setShowOwnPosts] = useState(false);
  const [showNotOwnPosts, setShowNotOwnPosts] = useState(false);
  const [showLikedByMe, setShowLikedByMe] = useState(false);

  const toggleShowOwnPosts = () => {
    setShowOwnPosts(!showOwnPosts);
    postsFilter.ignoreUser = false;
    postsFilter.ignoreOther = !showOwnPosts;
    postsFilter.from = 0;
    load(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleshowLikedByMe = () => {
    setShowLikedByMe(!showLikedByMe);
    postsFilter.likedByMe = !showLikedByMe;
    postsFilter.from = 0;
    load(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleShowNotOwnPosts = () => {
    setShowNotOwnPosts(!showNotOwnPosts);
    postsFilter.ignoreOther = false;
    postsFilter.ignoreUser = !showNotOwnPosts;
    postsFilter.from = 0;
    load(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const getMorePosts = () => {
    loadMore(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  };

  const sharePost = id => {
    setSharedPostId(id);
  };

  const uploadImage = file => imageService.uploadImage(file);

  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <AddPost addPost={createPost} uploadImage={uploadImage} />
      </div>
      <div className={styles.toolbar}>
        <Grid>
          <Grid.Column floated="left" width={8}>
            <Checkbox
              toggle
              label="Show only my posts"
              checked={showOwnPosts}
              disabled={showNotOwnPosts}
              onChange={toggleShowOwnPosts}
            />
          </Grid.Column>
          <Grid.Column floated="right" width={6}>
            <Checkbox
              toggle
              label="Hide my posts"
              checked={showNotOwnPosts}
              disabled={showOwnPosts}
              onChange={toggleShowNotOwnPosts}
            />
          </Grid.Column>
        </Grid>
      </div>
      <div>
        <Checkbox
          label="Show only posts liked by me"
          checked={showLikedByMe}
          onChange={toggleshowLikedByMe}
        />
      </div>
      <InfiniteScroll
        pageStart={0}
        loadMore={getMorePosts}
        hasMore={hasMorePosts}
        loader={<Loader active inline="centered" key={0} />}
      >
        {posts.map(post => (
          <Post
            post={post}
            likePost={like}
            dislikePost={dislike}
            toggleExpandedPost={toggle}
            toggleEditablePost={toggleEdit}
            sharePost={sharePost}
            key={post.id}
            currentUserId={userId}
            togglePostDelete={toggleDelete}
          />
        ))}
      </InfiniteScroll>
      {deletePostModal && <ModalDeletePost />}
      {editablePost && <EditablePost />}
      {expandedPost && <ExpandedPost sharePost={sharePost} currentUserId={userId} />}
      {sharedPostId && <SharedPostLink postId={sharedPostId} close={() => setSharedPostId(undefined)} />}
    </div>
  );
};

Thread.propTypes = {
  posts: PropTypes.arrayOf(PropTypes.object),
  hasMorePosts: PropTypes.bool,
  expandedPost: PropTypes.objectOf(PropTypes.any),
  editablePost: PropTypes.objectOf(PropTypes.any),
  deletePostModal: PropTypes.objectOf(PropTypes.any),
  userId: PropTypes.string,
  loadPosts: PropTypes.func.isRequired,
  loadMorePosts: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  toggleEditablePost: PropTypes.func.isRequired,
  togglePostDelete: PropTypes.func.isRequired,
  addPost: PropTypes.func.isRequired
};

Thread.defaultProps = {
  posts: [],
  hasMorePosts: true,
  expandedPost: undefined,
  editablePost: undefined,
  deletePostModal: undefined,
  userId: undefined
};

const mapStateToProps = rootState => ({
  posts: rootState.posts.posts,
  hasMorePosts: rootState.posts.hasMorePosts,
  expandedPost: rootState.posts.expandedPost,
  editablePost: rootState.posts.editablePost,
  deletePostModal: rootState.posts.deletePostModal,
  userId: rootState.profile.user.id
});

const actions = {
  loadPosts,
  loadMorePosts,
  likePost,
  dislikePost,
  toggleExpandedPost,
  toggleEditablePost,
  togglePostDelete,
  addPost
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Thread);

