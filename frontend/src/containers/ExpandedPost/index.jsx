import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Comment as CommentUI, Header } from 'semantic-ui-react';
import moment from 'moment';
import { likePost, likeComment, dislikeComment, dislikePost, toggleExpandedPost,
  toggleEditableComment, addComment, toggleCommentDelete } from 'src/containers/Thread/actions';
import ModalEditComment from 'src/components/ModalEditComment';
import ModalDeleteComment from 'src/components/ModalDeleteComment';
import Post from 'src/components/Post';
import Comment from 'src/components/Comment';
import AddComment from 'src/components/AddComment';
import Spinner from 'src/components/Spinner';

const ExpandedPost = ({
  post,
  currentUserId: thisId,
  sharePost,
  likePost: like,
  dislikePost: dislike,
  toggleExpandedPost: toggle,
  addComment: add,
  toggleEditableComment: toggleEditComment,
  editableComment,
  toggleCommentDelete: toggleCommentDel,
  deleteCommentModal,
  likeComment: likeCom,
  dislikeComment: dislikeCom
}) => (
  <Modal centered={false} closeIcon open onClose={() => toggle() /* dimmer="blurring"*/}>
    {post
      ? (
        <Modal.Content>
          <Post
            post={post}
            likePost={like}
            dislikePost={dislike}
            toggleExpandedPost={toggle}
            sharePost={sharePost}
          />
          <CommentUI.Group style={{ maxWidth: '100%' }}>
            <Header as="h3" dividing>
              Comments
            </Header>
            {post.comments && post.comments
              .sort((c1, c2) => moment(c1.createdAt).diff(c2.createdAt))
              .map(comment => (
                <Comment
                  key={comment.id}
                  id={comment.id}
                  likeComment={likeCom}
                  dislikeComment={dislikeCom}
                  comment={comment}
                  currentUserId={thisId}
                  toggleEditableComment={toggleEditComment}
                  toggleCommentDelete={toggleCommentDel}
                />
              ))}
            <AddComment postId={post.id} addComment={add} />
          </CommentUI.Group>
        </Modal.Content>
      )
      : <Spinner />}
    {deleteCommentModal && <ModalDeleteComment />}
    {editableComment && <ModalEditComment postId={post.id} />}
  </Modal>
);

ExpandedPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  toggleEditableComment: PropTypes.func,
  editableComment: PropTypes.objectOf(PropTypes.any),
  toggleCommentDelete: PropTypes.func,
  deleteCommentModal: PropTypes.objectOf(PropTypes.any),
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  addComment: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  currentUserId: PropTypes.string.isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired
};

ExpandedPost.defaultProps = {
  toggleEditableComment: undefined,
  toggleCommentDelete: undefined,
  editableComment: undefined,
  deleteCommentModal: undefined
};

const mapStateToProps = rootState => ({
  post: rootState.posts.expandedPost,
  editableComment: rootState.posts.editableComment,
  deleteCommentModal: rootState.posts.deleteCommentModal
});

const actions = {
  likePost,
  likeComment,
  dislikeComment,
  dislikePost,
  toggleExpandedPost,
  addComment,
  toggleEditableComment,
  toggleCommentDelete
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExpandedPost);
