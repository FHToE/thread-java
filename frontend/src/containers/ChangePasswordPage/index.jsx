import React from 'react';
import Logo from 'src/components/Logo';
import ChangePasswordForm from 'src/components/ChangePasswordForm';
import PropTypes from 'prop-types';
import { Grid, Header, Message } from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';

const ChangePasswordPage = ({ match }) => (
  <Grid textAlign="center" verticalAlign="middle" className="fill">
    <Grid.Column style={{ maxWidth: 450 }}>
      <Logo />
      <Header as="h2" color="teal" textAlign="center">
        Change password page
      </Header>
      <ChangePasswordForm id={match.params.resetLink} />
      <Message>
        Back to
        {' '}
        <NavLink exact to="/login">login page</NavLink>
      </Message>
    </Grid.Column>
  </Grid>
);

ChangePasswordPage.defaultProps = {
  match: undefined
};

ChangePasswordPage.propTypes = {
  match: PropTypes.objectOf(PropTypes.any)
};

export default ChangePasswordPage;
