import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getUserImgLink } from 'src/helpers/imageHelper';
import {
  Grid,
  Image,
  Input,
  Label,
  Icon,
  Button
} from 'semantic-ui-react';
import { bindActionCreators } from 'redux';
import * as imageService from 'src/services/imageService';
import * as userService from 'src/services/authService';
import ImageCompressor from 'image-compressor.js';
import ReactCrop from 'react-image-crop';
import StatusInputField from 'src/components/StatusInputField';
import { editUserData } from './actions';
import 'react-image-crop/dist/ReactCrop.css';
import styles from './styles.module.scss';

const Profile = ({ user, editUserData: edit }) => {
  const { username, email, image } = user;
  const [currentName, setCurrentName] = useState(username);
  const [currentUserEmail, setCurrentEmail] = useState(email);
  const [currentUserImg, setCurrentImg] = useState(image);

  const [isEditableMail, setIsEditableMail] = useState(false);
  const [isEditableName, setIsEditableName] = useState(false);

  const [newName, setName] = useState(currentName);
  const [newEmail, setEmail] = useState(currentUserEmail);
  const [isValidEmail, setisValidEmail] = useState(true);
  const [isValidName, setisValidName] = useState(true);

  function validateEmail(e) {
    const pattern = /^([A-Za-z0-9_\-.])+@([A-Za-z0-9_\-.])+\.([A-Za-z]{2,4})$/;
    setEmail(e.currentTarget.value);
    setisValidEmail(pattern.test(e.currentTarget.value));
  }

  const validateName = async e => {
    setName(e);
    const userList = await userService.getUserNames();
    const isExist = userList.map(u => u.username.toUpperCase())
      .filter(u => u !== currentName.toUpperCase()).includes(e.toUpperCase());
    setisValidName(!isExist);
  };

  const userNameElement = (
    <div>
      <Input
        icon="user"
        iconPosition="left"
        placeholder="Username"
        type="text"
        disabled={!isEditableName}
        value={newName}
        onChange={e => validateName(e.currentTarget.value)}
      />
      {!isEditableName
        ? (
          <Label
            basic
            size="tiny"
            as="a"
            className={styles.toolbarBtn}
            onClick={() => setIsEditableName(!isEditableName)}
          >
            <Icon color="black" name="pencil alternate">
              <div>edit...</div>
            </Icon>
          </Label>
        ) : (
          <Label
            basic
            size="tiny"
            as="a"
            className={styles.toolbarBtn}
            onClick={() => {
              setIsEditableName(!isEditableName);
              setName(currentName);
              setisValidName(true);
            }}
          >
            <Icon color="pink" name="undo">
              <div>cancel</div>
            </Icon>
          </Label>
        )}
      <div>{isValidName ? <br /> : <Label color="pink" size="mini">User with this name already exists</Label>}</div>
    </div>
  );

  const userEmailElement = (
    <div>
      <Input
        icon="at"
        iconPosition="left"
        placeholder="Email"
        type="email"
        disabled={!isEditableMail}
        value={newEmail}
        onChange={e => validateEmail(e)}
      />
      {!isEditableMail
        ? (
          <Label
            basic
            size="tiny"
            as="a"
            className={styles.toolbarBtn}
            onClick={() => setIsEditableMail(!isEditableMail)}
          >
            <Icon color="black" name="pencil alternate">
              <div>edit...</div>
            </Icon>
          </Label>
        ) : (
          <Label
            basic
            size="tiny"
            as="a"
            className={styles.toolbarBtn}
            onClick={() => {
              setIsEditableMail(!isEditableMail);
              setEmail(currentUserEmail);
              setisValidEmail(true);
            }}
          >
            <Icon color="pink" name="undo">
              <div>cancel</div>
            </Icon>
          </Label>
        )}
      <div>{isValidEmail ? <br /> : <Label color="pink" size="mini">Not valid email</Label>}</div>
    </div>
  );

  const [srcForCrop, setSrc] = useState(null);
  const [crop, setCrop] = useState({ unit: 'px', aspect: 1 / 1, width: 150, x: 50, y: 50 });
  const [imRef, setImRef] = useState(undefined);
  const [newImage, setImage] = useState(currentUserImg);
  const [isEditableImage, setIsEditableImage] = useState(false);

  const Avatar = () => {
    if (srcForCrop) {
      return (
        <ReactCrop
          style={{ width: '350px', height: '350px' }}
          imageStyle={{ width: '100%', height: '100%' }}
          src={srcForCrop}
          crop={crop}
          ruleOfThirds
          onImageLoaded={i => setImRef(i)}
          onChange={newCrop => setCrop(newCrop)}
        />
      );
    }
    return (
      <Image
        centered
        src={newImage ? newImage.link : getUserImgLink(currentUserImg)}
        size="medium"
        circular
      />
    );
  };

  const [isUploading, setIsUploading] = useState(false);

  const toggleEditImage = () => {
    setIsEditableImage(!isEditableImage);
    if (isEditableImage) {
      setImage(image);
      setSrc(null);
      setCrop({ unit: 'px', aspect: 1 / 1, width: 150, x: 50, y: 50 });
      setImRef(undefined);
    }
  };

  const uploadImage = file => imageService.uploadImage(file);

  const getCroppedImg = (thisim, thiscrop) => {
    const canvas = document.createElement('canvas');
    const scaleX = thisim.naturalWidth / thisim.width;
    const scaleY = thisim.naturalHeight / thisim.height;
    canvas.width = thiscrop.width;
    canvas.height = thiscrop.height;
    const ctx = canvas.getContext('2d');

    ctx.drawImage(
      thisim,
      thiscrop.x * scaleX,
      thiscrop.y * scaleY,
      thiscrop.width * scaleX,
      thiscrop.height * scaleY,
      0,
      0,
      thiscrop.width,
      thiscrop.height
    );

    return new Promise(resolve => {
      canvas.toBlob(blob => {
        resolve(blob);
      }, 'image/jpeg', 1);
    });
  };

  const handleChangeAvatar = async () => {
    setIsUploading(true);
    const options = {
      maxWidth: 200,
      maxHeight: 200,
      minWidth: 200,
      minHeight: 200,
      width: 200,
      height: 200,
      quality: 0.8
    };
    const imageCompressor = new ImageCompressor();
    const cropped = await getCroppedImg(imRef, crop);
    imageCompressor.compress(cropped, options)
      .then(async result => {
        const { id: imageId, link: imageLink } = await uploadImage(result);
        setImage({ id: imageId, link: imageLink });
        setSrc(null);
        setCrop({ unit: 'px', aspect: 1 / 1, width: 150, x: 50, y: 50 });
        setImRef(undefined);
      })
      .catch(err => {
        console.log(err);
      });
    setIsUploading(false);
  };

  const handleDeleteImage = () => {
    setSrc(null);
    setCrop({ unit: 'px', aspect: 1 / 1, width: 150, x: 50, y: 50 });
    setImRef(undefined);
    setImage(undefined);
    setCurrentImg(undefined);
  };

  const handleUploadCrop = e => {
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener('load', () => setSrc(reader.result));
      reader.readAsDataURL(e.target.files[0]);
    }
  };

  const inputImageElement = () => {
    if (!isEditableImage) {
      return (
        <Label
          content="upload image"
          onClick={toggleEditImage}
          basic
          size="large"
          as="a"
          className={styles.toolbarBtn}
        />
      );
    }
    return (
      <div>
        <Button.Group size="small">
          <Button icon onClick={handleDeleteImage} labelPosition="left" as="label">
            <Icon name="delete" />
            Delete
          </Button>
          <Button.Or />
          <Button color="teal" icon labelPosition="right" as="label">
            <Icon name="image" />
            Select file...
            <input name="image" type="file" hidden onChange={handleUploadCrop} />
          </Button>
        </Button.Group>
        <br />
        <br />
        <Button.Group size="tiny">
          <Button onClick={toggleEditImage} color="pink">Cancel</Button>
          <Button.Or />
          <Button color="green" as="label" onClick={handleChangeAvatar} loading={isUploading} disabled={!imRef}>
            Confirm!
          </Button>
        </Button.Group>
      </div>
    );
  };

  const [saveSuccessful, setsaveSuccessful] = useState(true);
  const saveErrorLabel = result => {
    if (result) {
      return '';
    }
    return (
      <Label color="pink" size="small">
        Ooops! Something was wrong. Saving error occured. Probably, such email or nickname already exists
      </Label>
    );
  };

  const saveAll = async () => {
    await edit({
      imgId: newImage?.id,
      id: user.id,
      email: newEmail,
      username: newName
    }).then(result => {
      if (!result) {
        setIsEditableName(true);
        setIsEditableMail(true);
        setsaveSuccessful(false);
      } else {
        setsaveSuccessful(true);
        setIsEditableMail(false);
        setIsEditableName(false);
        setIsEditableImage(false);
        setCurrentName(newName);
        setCurrentEmail(newEmail);
        setCurrentImg(newImage);
      }
    });
  };

  const saveButton = () => {
    const isChange = newName !== currentName || newEmail !== currentUserEmail
      || newImage?.link !== image?.link;
    if (isChange) {
      return <Button color="teal" disabled={!(isValidEmail && isValidName)} onClick={saveAll}>Save Changes</Button>;
    }
    return '';
  };

  return (
    <Grid container textAlign="center" style={{ paddingTop: 30 }}>
      <Grid.Column>
        {Avatar()}
        <br />
        {inputImageElement()}
        <br />
        {userNameElement}
        {userEmailElement}
        <br />
        {saveButton()}
        <br />
        {saveErrorLabel(saveSuccessful)}
        <br />
        <br />
        <br />
        My status:
        <StatusInputField user={user} />
      </Grid.Column>
    </Grid>
  );
};

Profile.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  editUserData: PropTypes.func.isRequired
};

Profile.defaultProps = {
  user: {}
};

const actions = { editUserData };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

const mapStateToProps = rootState => ({
  user: rootState.profile.user
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
