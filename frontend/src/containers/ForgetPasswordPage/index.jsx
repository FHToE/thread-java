import React from 'react';
import Logo from 'src/components/Logo';
import ResetPasswordForm from 'src/components/ResetPasswordForm';
import { Grid, Header, Message } from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';

const ForgetPasswordPage = () => (
  <Grid textAlign="center" verticalAlign="middle" className="fill">
    <Grid.Column style={{ maxWidth: 450 }}>
      <Logo />
      <Header as="h2" color="teal" textAlign="center">
        Get link on e-mail to reset password
      </Header>
      <ResetPasswordForm />
      <Message>
        Back to
        {' '}
        <NavLink exact to="/login">login page</NavLink>
      </Message>
    </Grid.Column>
  </Grid>
);

export default ForgetPasswordPage;
