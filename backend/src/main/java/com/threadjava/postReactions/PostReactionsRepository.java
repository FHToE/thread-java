package com.threadjava.postReactions;

import com.threadjava.comment.dto.CommentDetailsQueryResult;
import com.threadjava.postReactions.dto.LikeDetailsReactionDto;
import com.threadjava.postReactions.model.PostReaction;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PostReactionsRepository extends CrudRepository<PostReaction, UUID> {
    @Query("SELECT r " +
            "FROM PostReaction r " +
            "WHERE r.user.id = :userId AND r.post.id = :postId ")
    Optional<PostReaction> getPostReaction(@Param("userId") UUID userId, @Param("postId") UUID postId);

    @Query("SELECT new com.threadjava.postReactions.dto.LikeDetailsReactionDto(r.post.id, r.user, r.isLike, r.isDislike) " +
            "FROM PostReaction r " +
            "WHERE r.post.id = :postId AND (r.isLike = true OR r.isDislike = true) " +
            "order by r.createdAt desc" )
    List<LikeDetailsReactionDto> findLikeByPostId(@Param("postId") UUID postId);

}