package com.threadjava.postReactions.dto;

import com.threadjava.users.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LikeDetailsReactionDto {
    private UUID postId;
    private User user;
    private Boolean isLike;
    private Boolean isDislike;
}
