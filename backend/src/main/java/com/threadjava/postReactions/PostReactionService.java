package com.threadjava.postReactions;

import com.threadjava.postReactions.dto.ReceivedPostReactionDto;
import com.threadjava.postReactions.dto.ResponsePostReactionDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Optional;


@Service
public class PostReactionService {
    @Autowired
    private PostReactionsRepository postReactionsRepository;

    public Optional<ResponsePostReactionDto> setReaction(ReceivedPostReactionDto postReactionDto) {
        var reaction = postReactionsRepository.getPostReaction(postReactionDto.getUserId(), postReactionDto.getPostId());
        //look for reaction in database

        if (reaction.isPresent()) { //if reaction is already in database
            var react = reaction.get(); //reaction from sql

            if (react.getIsDislike() == null) {
                react.setIsDislike(false);
                postReactionsRepository.save(react);
            } // handling nulls in dislike columns (sql)

            if (postReactionDto.getIsLike() && !react.getIsLike()) {
                react.setIsLike(postReactionDto.getIsLike());
                var result = postReactionsRepository.save(react);
                return Optional.of(PostReactionMapper.MAPPER.reactionToPostReactionDto(result));
            } //case when post liked

            if (postReactionDto.getIsLike() && react.getIsLike()) {
                react.setIsLike(!postReactionDto.getIsLike());
                var result = postReactionsRepository.save(react);
                return Optional.of(PostReactionMapper.MAPPER.reactionToPostReactionDto(result));
            } // case when post unliked (undo like)

            if (postReactionDto.getIsDislike() && !react.getIsDislike()) {
                react.setIsDislike(postReactionDto.getIsDislike());
                var result = postReactionsRepository.save(react);
                return Optional.of(PostReactionMapper.MAPPER.reactionToPostReactionDto(result));
            } // case when post disliked

            if (postReactionDto.getIsDislike() && react.getIsDislike()) {
                react.setIsDislike(!postReactionDto.getIsDislike());
                var result = postReactionsRepository.save(react);
                return Optional.of(PostReactionMapper.MAPPER.reactionToPostReactionDto(result));
            }   // case when post undisliked (undo dislike)
            }
            var postReaction = PostReactionMapper.MAPPER.dtoToPostReaction(postReactionDto);
            var result = postReactionsRepository.save(postReaction);

            return Optional.of(PostReactionMapper.MAPPER.reactionToPostReactionDto(result));
    }
}
