package com.threadjava.commentReactions.dto;

import com.threadjava.users.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommentsReactionShortDto {
    private UUID commentId;
    private User user;
    private Boolean isLike;
    private Boolean isDislike;
}
