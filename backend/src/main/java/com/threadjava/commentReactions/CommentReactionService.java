package com.threadjava.commentReactions;

import com.threadjava.commentReactions.dto.ReceivedCommentReactionDto;
import com.threadjava.commentReactions.dto.ResponseCommentReactionDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CommentReactionService {
    @Autowired
    private CommentReactionsRepository commentReactionsRepository;

    public Optional<ResponseCommentReactionDto> setReaction(ReceivedCommentReactionDto commentReactionDto) {

        var reaction = commentReactionsRepository.getCommentReaction(commentReactionDto.getUserId(), commentReactionDto.getCommentId());
        //look for reaction in database

        if (reaction.isPresent()) { //if reaction is already in database
            var react = reaction.get(); //reaction from sql

            if (react.getIsDislike() == null) {
                react.setIsDislike(false);
                commentReactionsRepository.save(react);
            } // handling nulls in dislike columns (sql)

            if (commentReactionDto.getIsLike() && !react.getIsLike()) {
                react.setIsLike(commentReactionDto.getIsLike());
                var result = commentReactionsRepository.save(react);
                return Optional.of(CommentReactionMapper.MAPPER.reactionToCommentReactionDto(result));
            } //case when comment liked

            if (commentReactionDto.getIsLike() && react.getIsLike()) {
                react.setIsLike(!commentReactionDto.getIsLike());
                var result = commentReactionsRepository.save(react);
                return Optional.of(CommentReactionMapper.MAPPER.reactionToCommentReactionDto(result));
            } // case when comment unliked (undo like)

            if (commentReactionDto.getIsDislike() && !react.getIsDislike()) {
                react.setIsDislike(commentReactionDto.getIsDislike());
                var result = commentReactionsRepository.save(react);
                return Optional.of(CommentReactionMapper.MAPPER.reactionToCommentReactionDto(result));
            } // case when comment disliked

            if (commentReactionDto.getIsDislike() && react.getIsDislike()) {
                react.setIsDislike(!commentReactionDto.getIsDislike());
                var result = commentReactionsRepository.save(react);
                return Optional.of(CommentReactionMapper.MAPPER.reactionToCommentReactionDto(result));
            }   // case when comment undisliked (undo dislike)
            }
            var commentReaction = CommentReactionMapper.MAPPER.dtoToCommentReaction(commentReactionDto);
            var result = commentReactionsRepository.save(commentReaction);
            return Optional.of(CommentReactionMapper.MAPPER.reactionToCommentReactionDto(result));
    }
}
