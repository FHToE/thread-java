package com.threadjava.commentReactions;

import com.threadjava.commentReactions.dto.ReceivedCommentReactionDto;
import com.threadjava.commentReactions.dto.ResponseCommentReactionDto;
import com.threadjava.comment.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/commentreaction")
public class CommentReactionController {
    @Autowired
    private CommentReactionService reactionService;
    @Autowired
    private SimpMessagingTemplate template;
    @Autowired
    private CommentRepository commentRepository;

    @PutMapping
    public Optional<ResponseCommentReactionDto> setReaction(@RequestBody ReceivedCommentReactionDto commentReaction){
        commentReaction.setUserId(getUserId());
        var reaction = reactionService.setReaction(commentReaction);

        if (reaction.isPresent() && !reaction.get().getUserId().equals(getUserId())) {   //working variant
            //if (reaction.isPresent() && reaction.get().getUserId() != getUserId()) {  // test variant
            if (reaction.get().getIsLike().equals(commentReaction.getIsLike()) && reaction.get().getIsLike()) {
                // notify a user if someone liked his post
                template.convertAndSend("/topic/commentlike", commentRepository
                        .findCommentById(reaction.orElseThrow().getCommentId()).
                        orElseThrow().getUser().getId());
            }
            if (reaction.get().getIsDislike().equals(commentReaction.getIsDislike()) && reaction.get().getIsDislike()) {
                // notify a user if someone disliked his post
                template.convertAndSend("/topic/commentdislike", commentRepository
                        .findCommentById(reaction.orElseThrow().getCommentId()).
                                orElseThrow().getUser().getId());
            }
        };
        return reaction;
    }
}
