package com.threadjava.resetPassword;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.UUID;


@Data
public class ResetPasswordEntity {
    private UUID id;
    private UUID userId;
    private LocalDateTime createdAt;
}
