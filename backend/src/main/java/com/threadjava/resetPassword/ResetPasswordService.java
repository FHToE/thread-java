package com.threadjava.resetPassword;


import lombok.Data;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.UUID;
import java.time.LocalDateTime;

@Data
public class ResetPasswordService {
    private static ArrayList<ResetPasswordEntity> resets = new ArrayList<>();

    public static String getResetLink(UUID userId) {
        removeOld();
        resets.removeIf(e -> e.getUserId().equals(userId));
        ResetPasswordEntity entity = new ResetPasswordEntity();
        UUID id = UUID.randomUUID();
        entity.setId(id);
        entity.setUserId(userId);
        entity.setCreatedAt(LocalDateTime.now());
        resets.add(entity);
        return "http://localhost:3001/reset/" + id.toString();
    }

    public static UUID getUserIdAndRemove(UUID id) {
        removeOld();
        var entity = resets.stream().filter(e -> e.getId().equals(id)).findFirst();
        if (entity.isEmpty()) return null;
        resets.remove(entity.get());
        return entity.get().getUserId();
    }


    public static boolean checkIsValidLink(UUID id) {
        removeOld();
        return resets.stream().map(e->e.getId()).anyMatch(e->e.equals(id));
    }

    public static void removeOld() {
        ZoneId zoneId = ZoneId.systemDefault();
        resets.removeIf(e -> (LocalDateTime.now().atZone(zoneId).toEpochSecond()
                - e.getCreatedAt().atZone(zoneId).toEpochSecond()) > 320);
    }
}
