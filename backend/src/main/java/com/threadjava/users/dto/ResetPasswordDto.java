package com.threadjava.users.dto;

import lombok.Data;

@Data
public class ResetPasswordDto {
    private String email;
}
