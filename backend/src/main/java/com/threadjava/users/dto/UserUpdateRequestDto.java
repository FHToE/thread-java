package com.threadjava.users.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class UserUpdateRequestDto {
    private UUID id;
    private String email;
    private String username;
    private UUID imgId;
}
