package com.threadjava.users.dto;

import lombok.Data;
import java.util.UUID;

@Data
public class SavePasswordDto {
    private String password;
    private UUID resetId;
}
