package com.threadjava.users;

import com.threadjava.comment.dto.CommentDetailsQueryResult;
import com.threadjava.users.dto.UserShortDto;
import com.threadjava.users.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UsersRepository extends JpaRepository<User, UUID> {
    Optional<User> findByEmail(String email);

    @Query("SELECT new com.threadjava.users.dto.UserShortDto(u.id, u.username) " +
            "FROM User u")
    List<UserShortDto> findAllShortDto();

    @Transactional
    @Modifying
    @Query("UPDATE User u SET u.email = :email, u.avatar.id = :avatar_id, u.updatedAt = now(), " +
            "u.username = :username WHERE u.id = :id")
    void updateUsersDataById(@Param("id") UUID id, @Param("email") String email,
                             @Param("username") String username, @Param("avatar_id") UUID imageid);

    @Transactional
    @Modifying
    @Query("UPDATE User u SET u.status = :status WHERE u.id = :id")
    void updateUsersStatusId(@Param("id") UUID id, @Param("status") String status);

    @Transactional
    @Modifying
    @Query("UPDATE User u SET u.password = :password WHERE u.id = :id")
    void saveNewPasswordByUserId(@Param("id") UUID id, @Param("password") String password);
}