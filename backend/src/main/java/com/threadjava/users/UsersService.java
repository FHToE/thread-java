package com.threadjava.users;

import com.threadjava.auth.model.AuthUser;
import com.threadjava.users.dto.StatusUpdateDto;
import com.threadjava.users.dto.UserDetailsDto;
import com.threadjava.users.dto.UserUpdateRequestDto;
import com.threadjava.users.dto.UserUpdateResponseDto;
import com.threadjava.users.model.User;
import com.threadjava.email.EmailSender;
import com.threadjava.auth.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.UUID;

import static com.threadjava.resetPassword.ResetPasswordService.getResetLink;
import static com.threadjava.resetPassword.ResetPasswordService.getUserIdAndRemove;

@Service
public class UsersService implements UserDetailsService {
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private EmailSender emailSender;
    @Autowired
    private AuthService authService;

    @Override
    public AuthUser loadUserByUsername(String email) throws UsernameNotFoundException {
        return usersRepository
                .findByEmail(email)
                .map(user -> new AuthUser(user.getId(), user.getEmail(), user.getPassword()))
                .orElseThrow(() -> new UsernameNotFoundException(email));
    }

    public UserDetailsDto getUserById(UUID id) {
        return usersRepository
                .findById(id)
                .map(UserMapper.MAPPER::userToUserDetailsDto)
                .orElseThrow(() -> new UsernameNotFoundException("No user found with username"));
    }

    public Boolean getResetPasswordLink(String email) {
        var user = usersRepository.findByEmail(email);
        if (user.isEmpty()) return false;
        String link = getResetLink(user.get().getId());
        try {
            emailSender.resetPassword(link, email);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    public Boolean saveNewPassword(String password, UUID resetId) {
        UUID userId = getUserIdAndRemove(resetId);
        if (userId == null) return false;
        String codedPassword = authService.getBCryptPasswordEncoder().encode(password);
        usersRepository.saveNewPasswordByUserId(userId, codedPassword);
        return true;
    }

    public UserUpdateResponseDto update(UserUpdateRequestDto userDto) {
        boolean flag;
        synchronized (this) {
            flag = usersRepository.findAll().stream().filter(u-> !u.getId().equals(userDto.getId()))
                    .anyMatch(u-> (u.getEmail().equalsIgnoreCase(userDto.getEmail()) || u.getUsername()
                    .equalsIgnoreCase(userDto.getUsername())));
            if (!flag) {
                usersRepository.updateUsersDataById(userDto.getId(), userDto.getEmail(), userDto.getUsername(), userDto.getImgId());
            }}
            var response = UserMapper.MAPPER.userToUserUpdateResponseDto(usersRepository.findById(userDto.getId()).orElseThrow());
            response.setSuccessful(!flag);
            return response;
    }

    public void save(User user) {
        usersRepository.save(user);
    }

    public void updateStatus(StatusUpdateDto statusRequest) {
        usersRepository.updateUsersStatusId(statusRequest.getId(), statusRequest.getStatus());
    }
}