package com.threadjava.users;

import com.threadjava.users.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

import static com.threadjava.auth.TokenService.getUserId;
import static com.threadjava.resetPassword.ResetPasswordService.checkIsValidLink;

@RestController
@RequestMapping("/api/user")
public class UserController {
    @Autowired
    private UsersService userDetailsService;
    @Autowired
    private UsersRepository repository;

    @GetMapping
    public UserDetailsDto getUser() {
        return userDetailsService.getUserById(getUserId());
    }

    @GetMapping("/getall")
    public List<UserShortDto> getAll() {
        return repository.findAllShortDto();
    }

    @PostMapping("/getreset")
    public Boolean getResetLink(@RequestBody ResetPasswordDto request) {
        return userDetailsService.getResetPasswordLink(request.getEmail());
    }

    @GetMapping("/getreset/{id}")
    public Boolean isValidResetLink(@PathVariable UUID id) {
        return checkIsValidLink(id);
    }

    @PutMapping("/getreset")
    public Boolean savePassword(@RequestBody SavePasswordDto request) {
        return userDetailsService.saveNewPassword(request.getPassword(), request.getResetId());
    }

    @PostMapping("/{id}")
    public UserUpdateResponseDto update(@RequestBody UserUpdateRequestDto request) {
        return userDetailsService.update(request);
    }

    @PostMapping("/status")
    public void updateStatus(@RequestBody StatusUpdateDto request) {
        userDetailsService.updateStatus(request);
    }
}
