package com.threadjava.email;

import com.threadjava.email.dto.LikePostEmailDto;
import com.threadjava.email.dto.SharePostEmailDto;
import com.threadjava.post.dto.PostEditDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/email/")
public class EmailController {
    @Autowired
    private EmailSender emailSender;

    @PostMapping("/{id}")
    public void likedPost(@RequestBody LikePostEmailDto request) {
        try {
            emailSender.sendPostLiked(request.getPostId(), request.getPostLikedById(), request.getPostOwnerId());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @PostMapping("/sharepost")
    public void sharePost(@RequestBody SharePostEmailDto request) {
        try {
            emailSender.SharePost(request.getPostLink(), request.getReceiversId());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
