package com.threadjava.email;

import com.threadjava.users.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import java.util.UUID;

import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import static com.threadjava.auth.TokenService.getUserId;

@Component
@PropertySource("classpath:config.properties")
public class EmailSender {
    @Value(value = "${email.from}")
    private String FROM;
    @Value(value = "${aws.username}")
    private String SMTP_USERNAME;
    @Value(value = "${aws.password}")
    private String SMTP_PASSWORD;
    @Value(value = "${name.emailFrom}")
    private String FROMNAME;
    @Value(value = "${aws.configset}")
    private String CONFIGSET;
    final String HOST = "email-smtp.us-west-2.amazonaws.com";
    final int PORT = 587;

    @Autowired
    private UsersService userService;

    private Properties getProperties() {
        Properties props = System.getProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.port", PORT);
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.ssl.trust", "*");
        props.put("mail.smtp.auth", "true");
        return props;
    }

    private MimeMessage getMessage(Session session, String subject, String body, String... mailTo) {
        MimeMessage msg = new MimeMessage(session);
        try {
            for (String adress : mailTo) {
                msg.addRecipient(Message.RecipientType.TO, new InternetAddress(adress));
            }
            //msg.setRecipient(Message.RecipientType.TO, new InternetAddress(mailTo));
            msg.setFrom(new InternetAddress(FROM,FROMNAME));
            msg.setSubject(subject);
            msg.setContent(body,"text/html");
            msg.setHeader("X-SES-CONFIGURATION-SET", CONFIGSET);
        } catch (MessagingException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return msg;
    }

    public void SharePost(String postLink, UUID[] receiversId) throws Exception {
        if (postLink == null || receiversId == null) return;
        Session session = Session.getDefaultInstance(getProperties());
        Transport transport = session.getTransport();
        String name = userService.getUserById(getUserId()).getUsername();
        String subject = "Someone share post with you";
        String body = String.join(
                System.getProperty("line.separator"),
                "<h1>Hello.</h1>",
                "<p><strong>" + name + "</strong> shared a post with you.</p>",
                "<p>Have a look: <a href=\"" + postLink + "\">post.</a></p>",
                "<br />");
        String [] emails = new String[receiversId.length];
        for(int i = 0; i < receiversId.length ; i++) {
            emails[i] = userService.getUserById(receiversId[i]).getEmail();
        }
        MimeMessage msg = getMessage(session, subject, body, emails);
        try
        {
            System.out.println("Sending...");
            transport.connect(HOST, SMTP_USERNAME, SMTP_PASSWORD);
            transport.sendMessage(msg, msg.getAllRecipients());
            System.out.println("Email sent!");
        }
        catch (Exception ex) {
            System.out.println("The email was not sent.");
            System.out.println("Error message: " + ex.getMessage());
        }
        finally
        {
            transport.close();
        }
    }

    public void resetPassword(String link, String email) throws Exception {
        Session session = Session.getDefaultInstance(getProperties());
        Transport transport = session.getTransport();
        String resetLink = "<p><a href=\"" + link + "\">Reset.</a></p>";
        String subject = "Reset Password";
        String body = String.join(
                System.getProperty("line.separator"),
                "<h1>Hello.</h1>",
                "<p>You requested reset your password.</p>",
                "<p>Link time expires in 5 minutes. Your link: </p>",
                resetLink,
                "<br />");
        MimeMessage msg = getMessage(session, subject, body, email);
        try
        {
            System.out.println("Sending...");
            transport.connect(HOST, SMTP_USERNAME, SMTP_PASSWORD);
            transport.sendMessage(msg, msg.getAllRecipients());
            System.out.println("Email sent!");
        }
        catch (Exception ex) {
            System.out.println("The email was not sent.");
            System.out.println("Error message: " + ex.getMessage());
        }
        finally
        {
            transport.close();
        }
    }

    public void sendPostLiked(UUID postId, UUID likedById, UUID postOwnerId) throws Exception {
        if (postId == null || likedById == null || postOwnerId == null) return;
        Session session = Session.getDefaultInstance(getProperties());
        Transport transport = session.getTransport();
        String email = userService.getUserById(postOwnerId).getEmail();
        String likedByName = userService.getUserById(likedById).getUsername();
        String postLink = "<p><a href=\"http://localhost:3001/share/" + postId.toString() + "\">Post.</a></p>";
        String subject = "Post reaction";
        String body = String.join(
                System.getProperty("line.separator"),
                "<h1>Hello.</h1>",
                "<p>Your post was liked by <strong>" + likedByName + "</strong>.</p>",
                postLink,
                "<br />");
        MimeMessage msg = getMessage(session, subject, body, email);
        try
        {
            //System.out.println("Sending...");
            transport.connect(HOST, SMTP_USERNAME, SMTP_PASSWORD);
            transport.sendMessage(msg, msg.getAllRecipients());
            //System.out.println("Email sent!");
        }
        catch (Exception ex) {
            //System.out.println("The email was not sent.");
            //System.out.println("Error message: " + ex.getMessage());
        }
        finally
        {
            transport.close();
        }
    }
}
