package com.threadjava.email.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class SharePostEmailDto {
    private String postLink;
    private UUID [] receiversId;
}
