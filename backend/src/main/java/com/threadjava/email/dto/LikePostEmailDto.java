package com.threadjava.email.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class LikePostEmailDto {
    private UUID postId;
    private UUID postOwnerId;
    private UUID postLikedById;
}
