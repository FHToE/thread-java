package com.threadjava.post;

import com.threadjava.comment.CommentRepository;
import com.threadjava.postReactions.PostReactionsRepository;
import com.threadjava.post.dto.*;
import com.threadjava.post.model.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import com.threadjava.comment.CommentService;
import java.util.List;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class PostsService {
    @Autowired
    private PostsRepository postsCrudRepository;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private PostReactionsRepository reactionsRepository;
    @Autowired
    private CommentService commentService;

    public List<PostListDto> getAllPosts(Integer from, Integer count, UUID userId,
                                         Boolean ignoreOther, Boolean ignoreUser, Boolean likedByMe) {

        var pageable = PageRequest.of(from / count, count);

        return postsCrudRepository.findAllPosts(null, pageable)
                .stream()
                .map(setReactions)
                .filter(p->showUsersPostsFilter(userId, p, ignoreOther))
                .filter(p->ignoreUsersPostsFilter(userId, p, ignoreUser))
                .filter(p->likedByUserFilter(userId, p, likedByMe))
                .collect(Collectors.toList());
    }

    public PostDetailsDto getPostById(UUID id) {
        var post = postsCrudRepository.findPostById(id)
                .map(PostMapper.MAPPER::postToPostDetailsDto)
                .orElseThrow();

        var comments = commentService.getCommentsByPostId(id)
                .stream()
                .map(PostMapper.MAPPER::commentPlusReactionsToCommentDto)
                .collect(Collectors.toList());

        post.setComments(comments);

        /*var reactions = reactionsRepository.findLikeByPostId(id)
                .stream()
                .map(PostMapper.MAPPER::reactionToPostReactionDto)
                .collect(Collectors.toList());*/
        var reactions = getReactionsByPostId(id);
        post.setReactions(reactions);
        return post;
    }

    public List<PostReactionDto> getReactionsByPostId(UUID id) {
        return reactionsRepository.findLikeByPostId(id)
                .stream()
                .map(PostMapper.MAPPER::reactionToPostReactionDto)
                .collect(Collectors.toList());
    }

    public PostCreationResponseDto create(PostCreationDto postDto) {
        Post post = PostMapper.MAPPER.postDetailsDtoToPost(postDto);
        Post postCreated = postsCrudRepository.save(post);
        return PostMapper.MAPPER.postToPostCreationResponseDto(postCreated);
    }

    public PostCreationResponseDto update(PostEditDto postDto) {
       postsCrudRepository.updatePostBodyAndImageById(postDto.getId(), postDto.getBody(), postDto.getImageId());
       PostDetailsQueryResult updatedPost = postsCrudRepository.findPostById(postDto.getId()).orElseThrow();
       return PostMapper.MAPPER.postQueryResultToPostCreationResponse(updatedPost);
    }

    //helper methods
    private boolean likedByUserFilter(UUID userId, PostListDto post, Boolean toggleFilter) {
        if (toggleFilter==null || !toggleFilter) return true;
        return post.getReactions()
                .stream()
                .filter(r->r.getIsLike())
                .map(r->r.getUserId()).anyMatch(id -> id.equals(userId));
    }

    private boolean showUsersPostsFilter(UUID userId, PostListDto post, Boolean toggleFilter) {
        if (toggleFilter==null || !toggleFilter) return true;
        return post.getUser().getId().equals(userId);
    }

    private boolean ignoreUsersPostsFilter(UUID userId, PostListDto post, Boolean toggleFilter) {
        if (toggleFilter==null || !toggleFilter) return true;
        return !post.getUser().getId().equals(userId);
    }

    private Function<PostListQueryResult, PostListDto> setReactions = x -> {
        PostListDto p = PostMapper.MAPPER.postListToPostListDto(x);
        var reactions = reactionsRepository.findLikeByPostId(p.getId())
                .stream()
                .map(PostMapper.MAPPER::reactionToPostReactionDto)
                .collect(Collectors.toList());
        p.setReactions(reactions);
        return p;
    };

}
