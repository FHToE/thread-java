package com.threadjava.post;


import com.threadjava.post.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/posts")
public class PostsController {
    @Autowired
    private PostsService postsService;
    @Autowired
    private SimpMessagingTemplate template;
    @Autowired
    private PostsRepository postsCrudRepository;

    @GetMapping
    public List<PostListDto> get(@RequestParam(defaultValue="0") Integer from,
                                 @RequestParam(defaultValue="10") Integer count,
                                 @RequestParam Boolean ignoreOther,
                                 @RequestParam Boolean ignoreUser,
                                 @RequestParam Boolean likedByMe) {
        var userId = getUserId();
        return postsService.getAllPosts(from, count, userId, ignoreOther, ignoreUser, likedByMe);
    }

    @GetMapping("/{id}")
    public PostDetailsDto get(@PathVariable UUID id) {
        return postsService.getPostById(id);
    }

    @GetMapping("/reactions/{id}")
    public List<PostReactionDto> getReactions(@PathVariable UUID id) {
        return postsService.getReactionsByPostId(id);
    }

    @PostMapping
    public PostCreationResponseDto post(@RequestBody PostCreationDto postDto) {
        postDto.setUserId(getUserId());
        var item = postsService.create(postDto);
        template.convertAndSend("/topic/new_post", item);
        return item;
    }

    //my methods
    @PostMapping("/{id}")
    public PostCreationResponseDto postUpdate(@RequestBody PostEditDto postDto) {
        postDto.setUserId(getUserId());
        var item = postsService.update(postDto);
        template.convertAndSend("/topic/edit_post", getUserId());
        return item;
    }

    @PostMapping("/delete/{id}")
    public void deletePost(@PathVariable UUID id) {
        postsCrudRepository.deletePostById(id);
        template.convertAndSend("/topic/delete_post", getUserId());
    }
}
