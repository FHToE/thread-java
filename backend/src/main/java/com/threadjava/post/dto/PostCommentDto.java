package com.threadjava.post.dto;

import com.threadjava.comment.dto.CommentReactionDetailDto;
import lombok.Data;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Data
public class PostCommentDto {
    private UUID id;
    private String body;
    private PostUserDto user;
    private Date createdAt;
    public long likeCount;
    public long dislikeCount;
    private List<CommentReactionDetailDto> reactions;
}
