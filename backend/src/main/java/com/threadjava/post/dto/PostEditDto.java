package com.threadjava.post.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class PostEditDto {
    private String body;
    private UUID imageId;
    private UUID userId;
    private UUID id;
}