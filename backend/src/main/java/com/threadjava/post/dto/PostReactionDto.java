package com.threadjava.post.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class PostReactionDto {
    private UUID postId;
    private UUID userId;
    private String userName;
    private String userAvatarLink;
    private Boolean isLike;
    private Boolean isDislike;
}
