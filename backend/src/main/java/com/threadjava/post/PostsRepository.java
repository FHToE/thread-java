package com.threadjava.post;

import com.threadjava.post.dto.PostDetailsQueryResult;
import com.threadjava.post.model.Post;
import com.threadjava.post.dto.PostListQueryResult;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PostsRepository extends JpaRepository<Post, UUID> {

    @Query("SELECT new com.threadjava.post.dto.PostListQueryResult(p.id, p.body, " +
            "(SELECT COALESCE(SUM(CASE WHEN pr.isLike = TRUE THEN 1 ELSE 0 END), 0) FROM p.reactions pr WHERE pr.post = p), " +
            "(SELECT COALESCE(SUM(CASE WHEN pr.isDislike = TRUE THEN 1 ELSE 0 END), 0) FROM p.reactions pr WHERE pr.post = p), " +
            "(SELECT COUNT(*) FROM p.comments c WHERE c.isDelete = null OR c.isDelete = false), " +
            "p.createdAt, p.updatedAt, i, p.user) " +
            "FROM Post p " +
            "LEFT JOIN p.image i " +
            "WHERE ( cast(:userId as string) is null OR p.user.id = :userId) AND (p.isDelete = null OR p.isDelete = false) " +
            "order by p.createdAt desc" )
    List<PostListQueryResult> findAllPosts(@Param("userId") UUID userId, Pageable pageable);

    @Query("SELECT new com.threadjava.post.dto.PostDetailsQueryResult(p.id, p.body, " +
            "(SELECT COALESCE(SUM(CASE WHEN pr.isLike = TRUE THEN 1 ELSE 0 END), 0) FROM p.reactions pr WHERE pr.post = p), " +
            "(SELECT COALESCE(SUM(CASE WHEN pr.isDislike = TRUE THEN 1 ELSE 0 END), 0) FROM p.reactions pr WHERE pr.post = p), " +
            "(SELECT COUNT(*) FROM p.comments c WHERE c.isDelete = null OR c.isDelete = false), " +
            "p.createdAt, p.updatedAt, i, p.user) " +
            "FROM Post p " +
            "LEFT JOIN p.image i " +
            "WHERE p.id = :id AND (p.isDelete = null OR p.isDelete = false)")
    Optional<PostDetailsQueryResult> findPostById(@Param("id") UUID id);

    //my query
    @Transactional
    @Modifying
    @Query("UPDATE Post p SET p.body = :body, p.image.id = :imageid, p.updatedAt = now() WHERE p.id = :id")
    void updatePostBodyAndImageById(@Param("id") UUID id, @Param("body") String newBody, @Param("imageid") UUID imageid);


    @Transactional
    @Modifying
    @Query("UPDATE Post p SET p.isDelete = true, p.updatedAt = now() WHERE p.id = :id")
    void deletePostById(@Param("id") UUID id);
}