package com.threadjava.comment;

import com.threadjava.comment.dto.CommentDetailsDto;
import com.threadjava.comment.dto.CommentDetailsQueryResult;
import com.threadjava.comment.dto.CommentUpdateQueryResult;
import com.threadjava.comment.model.Comment;
import com.threadjava.post.dto.PostDetailsQueryResult;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface CommentRepository extends JpaRepository<Comment, UUID> {


    @Query("SELECT new com.threadjava.comment.dto.CommentDetailsQueryResult(c.id, c.body, c.user, " +
            "c.post.id, c.createdAt, c.updatedAt, c.isDelete, " +
            "(SELECT COALESCE(SUM(CASE WHEN cr.isLike = TRUE THEN 1 ELSE 0 END), 0) FROM c.reactions cr WHERE cr.comment = c), " +
            "(SELECT COALESCE(SUM(CASE WHEN cr.isDislike = TRUE THEN 1 ELSE 0 END), 0) FROM c.reactions cr WHERE cr.comment = c)) " +
            "FROM Comment c " +
            "WHERE ( c.post.id = :postId) AND (c.isDelete = null OR c.isDelete = false) " +
            "order by c.createdAt" )
    List<CommentDetailsQueryResult> findAllByPostId(@Param("postId") UUID postId);

    @Query("SELECT new com.threadjava.comment.dto.CommentDetailsQueryResult(c.id, c.body, c.user, " +
            "c.post.id, c.createdAt, c.updatedAt, c.isDelete, " +
            "(SELECT COALESCE(SUM(CASE WHEN cr.isLike = TRUE THEN 1 ELSE 0 END), 0) FROM c.reactions cr WHERE cr.comment = c), " +
            "(SELECT COALESCE(SUM(CASE WHEN cr.isDislike = TRUE THEN 1 ELSE 0 END), 0) FROM c.reactions cr WHERE cr.comment = c)) " +
            "FROM Comment c " +
            "WHERE ( c.id = :id) AND (c.isDelete = null OR c.isDelete = false)" )
    CommentDetailsQueryResult findCommentDetailsById (@Param("id") UUID id);
    // List<Comment> findAllByPostId(UUID postId);

    @Query("SELECT new com.threadjava.comment.dto.CommentUpdateQueryResult(c.id, c.body, " +
            "c.user, c.post.id) " +
            "FROM Comment c " +
            "WHERE c.id = :id")
    Optional<CommentUpdateQueryResult> findCommentById(@Param("id") UUID id);

    @Transactional
    @Modifying
    @Query("UPDATE Comment c SET c.body = :body, c.updatedAt = now() WHERE c.id = :id")
    void updateCommentBodyById(@Param("id") UUID id, @Param("body") String newBody);


    @Transactional
    @Modifying
    @Query("UPDATE Comment c SET c.isDelete = true, c.updatedAt = now() WHERE c.id = :id")
    void deleteCommentById(@Param("id") UUID id);
}