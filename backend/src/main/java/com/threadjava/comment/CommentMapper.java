package com.threadjava.comment;

import com.threadjava.comment.dto.*;
import com.threadjava.commentReactions.dto.CommentsReactionShortDto;
import com.threadjava.commentReactions.model.CommentReaction;
import com.threadjava.comment.model.Comment;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;



import org.mapstruct.factory.Mappers;

@Mapper
public interface CommentMapper {
    CommentMapper MAPPER = Mappers.getMapper(CommentMapper.class);

    @Mapping(source = "post.id", target = "postId")
    @Mapping(expression = "java(comment.reactions.stream().filter(r -> r.isLike).count())", target = "likeCount")
    @Mapping(expression = "java(comment.reactions.stream().filter(r -> r.isDislike).count())", target = "dislikeCount")
    CommentDetailsDto commentToCommentDetailsDto(Comment comment);

    @Mapping(source = "postId", target = "post.id")
    @Mapping(source = "userId", target = "user.id")
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "isDelete", ignore = true)
    @Mapping(target = "reactions", ignore = true)
    Comment commentSaveDtoToModel(CommentSaveDto commentDto);

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "user.username", target = "userName")
    @Mapping(source = "user.avatar.link", target = "userAvatarLink")
    CommentReactionDetailDto reactionToCommentReactionDetail(CommentsReactionShortDto reaction);

    @Mapping(target = "reactions", ignore = true)
    CommentPlusReactions commentToCommentPlusReactions(CommentDetailsQueryResult comment);
}
