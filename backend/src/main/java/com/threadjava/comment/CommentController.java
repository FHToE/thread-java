package com.threadjava.comment;

import com.threadjava.comment.dto.*;
import com.threadjava.commentReactions.dto.CommentsReactionShortDto;
import com.threadjava.post.PostsRepository;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;
import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/comments")
public class CommentController {
    @Autowired
    private CommentService commentService;
    @Autowired
    private SimpMessagingTemplate template;
    @Autowired
    private CommentRepository commentsCrudRepository;
    @Autowired
    private PostsRepository PostsCrudRepository;

    @GetMapping("/{id}")
    public CommentPlusReactions get(@PathVariable UUID id) {
        return commentService.getCommentPlusReactionsById(id);
    }

    @GetMapping("reactions/{id}")
    public List<CommentReactionDetailDto> getReactions(@PathVariable UUID id) {
        return commentService.getCommentReactions(id);
    }

    @PostMapping
    public CommentDetailsDto post(@RequestBody CommentSaveDto commentDto) {
        commentDto.setUserId(getUserId());
        var comment = commentService.create(commentDto);
        if (!comment.getUser().getId().equals(getUserId())) {
            template.convertAndSend("/topic/add_comment", PostsCrudRepository
                    .findPostById(comment.getPostId()).orElseThrow().getUser().getId());
        }
        return comment;
    }

    @PostMapping("/{id}")
    public CommentUpdateQueryResult updateComment(@RequestBody CommentUpdateDto commentDto) {
        commentDto.setUserId(getUserId());
        var item = commentService.update(commentDto);
        template.convertAndSend("/topic/edit_comment", getUserId());
        return item;
    }

    @PostMapping("/delete/{id}")
    public void deleteComment(@PathVariable UUID id) {
        commentsCrudRepository.deleteCommentById(id);
        template.convertAndSend("/topic/delete_comment", getUserId());
    }
}
