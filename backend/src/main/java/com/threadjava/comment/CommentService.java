package com.threadjava.comment;

import com.threadjava.comment.dto.*;
import com.threadjava.commentReactions.dto.CommentsReactionShortDto;
import com.threadjava.commentReactions.CommentReactionsRepository;
import com.threadjava.post.dto.PostCommentDto;
import com.threadjava.post.dto.PostListDto;
import com.threadjava.post.dto.PostListQueryResult;
import com.threadjava.users.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class CommentService {
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private CommentReactionsRepository reactionsRepository;
    /* @Autowired
    private PostsRepository postsRepository; */

    public CommentDetailsDto getCommentById(UUID id) {
        return commentRepository.findById(id)
                .map(CommentMapper.MAPPER::commentToCommentDetailsDto)
                .orElseThrow();
    }

    public CommentPlusReactions getCommentPlusReactionsById(UUID id) {
        return setReactions.apply(commentRepository.findCommentDetailsById(id));
    }

    public List<CommentReactionDetailDto> getCommentReactions(UUID id) {
        return reactionsRepository.getReactionsByCommentId(id)
                .stream()
                .map(CommentMapper.MAPPER::reactionToCommentReactionDetail)
                .collect(Collectors.toList());
    }

    public List<CommentPlusReactions> getCommentsByPostId(UUID id) {
        return commentRepository.findAllByPostId(id).stream().map(setReactions).collect(Collectors.toList());
    }

    private Function<CommentDetailsQueryResult, CommentPlusReactions> setReactions = x -> {
        CommentPlusReactions comment = CommentMapper.MAPPER.commentToCommentPlusReactions(x);
        var reactions = getCommentReactions(x.getId());
        comment.setReactions(reactions);
        return comment;
    };

    public CommentDetailsDto create(CommentSaveDto commentDto) {
        var comment = CommentMapper.MAPPER.commentSaveDtoToModel(commentDto);
        var commentCreated = commentRepository.save(comment);
        return CommentMapper.MAPPER.commentToCommentDetailsDto(commentCreated);
    }

    public CommentUpdateQueryResult update(CommentUpdateDto commentDto) {
        commentRepository.updateCommentBodyById(commentDto.getId(), commentDto.getBody());
        CommentUpdateQueryResult updatedComment = commentRepository.findCommentById(commentDto.getId()).orElseThrow();
        return updatedComment;
    }
}
