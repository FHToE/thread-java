package com.threadjava.comment.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class CommentUpdateDto {
    private String body;
    private UUID postId;
    private UUID userId;
    private UUID id;
}
