package com.threadjava.comment.dto;

import com.threadjava.users.model.User;
import lombok.Data;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Data
public class CommentPlusReactions {
    private UUID id;
    private String body;
    private User user;
    private UUID postId;
    private Date createdAt;
    private Date updatedAt;
    private Boolean isDelete;
    private long likeCount;
    private long dislikeCount;
    private List<CommentReactionDetailDto> reactions;
}
