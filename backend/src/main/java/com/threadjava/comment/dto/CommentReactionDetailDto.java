package com.threadjava.comment.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class CommentReactionDetailDto {
    private UUID commentId;
    private UUID userId;
    private String userName;
    private String userAvatarLink;
    private Boolean isLike;
    private Boolean isDislike;
}
